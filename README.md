
# Package managers #

Packages to install:

- git
- wget
- octave
- r
- texlive (some version of latex)
- [*] python
- [*] emacs

[*] _python_: for windows, pythonxy, Anaconda, or Enthought python distributions may be sufficient.

[*] _emacs_: I normally use the binary installer for emacs on mac. For cygwin, I install emacs and emacs-w32. Apparently, it is possible to get the native windows binary for GNU Emacs also, and use the cygwin bash for the shell.

List of package managers that I use:

- ubuntu: apt-get
- mac: macports
- windows: cygwin (cygwin64)

## macports ##

Common commands:

```sh
sudo port install [packagename]
sudo port selfupdate
sudo port upgrade outdated
sudo port uninstall active
sudo port select --set python python27
sudo port installed
```

Shell variables in `~/.bashrc`, `~/.bash_profile`, or `~/.profile` (should be set automatically by macports):

```sh
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
```

(Include `source` call to custom settings, including python paths with script below.)

## apt-get or aptitude ##

Need to check:

- python 
- python-numpy
- python-scipy
- python-pandas

## cygwin64 ##

Packages:

- openssh
- gcc-fortran
- gcc-core
- mingw-gcc-core
- gcc-g++
- ming-gcc-g++
- libgcc1
- r-core
- emacs
- emacs-w32

# Python #

## cygwin64 ##

```sh
python
python-setuptools
python-numpy
```

Additionally, BLAS and LAPACK need to be installed for scipy. More details, including alternative fortran compilers are located [here](http://stackoverflow.com/questions/7496547/python-scipy-needs-blas), from which the code below is taken.

### BLAS installation ###
```sh
mkdir -p ~/src/
cd ~/src/
wget http://www.netlib.org/blas/blas.tgz
tar xzf blas.tgz
cd BLAS
gfortran -O3 -std=legacy -m64 -fno-second-underscore -fPIC -c *.f
ar r libfblas.a *.o
ranlib libfblas.a
rm -rf *.o
export BLAS=~/src/BLAS/libfblas.a
```

### LAPACK installation ###
```sh
mkdir -p ~/src
cd ~/src/
wget http://www.netlib.org/lapack/lapack.tgz
tar xzf lapack.tgz
mv lapack-* lapack
cd lapack/
cp INSTALL/make.inc.gfortran make.inc
```

Stop to edit `make.inc`; add `-m64` flag to `OPTS` and `NOOPT`. Continue.

```sh
make lapacklib
make clean
export LAPACK=~/src/lapack/liblapack.a 
```

Note there is not an `f` in `liblapack.a` as in stackoverflow example.

### Python modules ###

Does not require sudo with admin privileges.

```sh
easy_install pip
pip install scipy
pip install pandas
pip install functional
```

### Emacs integration ###

For emacs to [recognize location of python and cygwin executables](http://stackoverflow.com/questions/9167614/python-mode-in-emacs-no-such-file-or-directory-pdb),

```common-lisp
(when (eq system-type 'cygwin)
  (setenv "PATH" (concat "/usr/bin:" (getenv "PATH")))
  (setq exec-path (split-string (getenv "PATH") path-separator)))
```

## macports ##

These are packages I can have installed (some dependencies, e.g. pandas and openbabel, require XCode command-line tools to be installed, which has to be obtained through the Apple App store):

```sh
python27
python_select
py27-numpy
py27-scipy
py27-setuptools
py27-pandas
py27-ipython
py27-matplotlib
py27-openbabel
```

### symbolic links ###

Links on my machine:

```sh
/usr/bin/python@ -> /opt/local/bin/python
/opt/local/bin/python@ -> /opt/local/bin/python2.7
/opt/local/bin/python2.7@ -> /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/python2.7
/opt/local/bin/ipython@ -> /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/ipython
/opt/local/bin/ipython-2.7@ -> /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/ipython
```

Might have to manually create this link:

```sh
ln -svf /opt/local/bin/ipython-2.7 /opt/local/bin/ipython
```

Most of the other links are created by macports, except for `/usr/bin/python` (see script below).

### shell variables ###

Shell variables to be included in `~/.bashrc` (can reference through script below):

```sh
export PATH=/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin:$PATH
export PYTHONPATH=/opt/local/Library/Frameworks/Python.framework/Versions/2.7/site-packages:$PYTHONPATH
```

A separate configuration file is necessary to make shell variables are available to other programs that are called as GUI, like Emacs. In `~/.launchd.conf` (supercedes `~/.MacOSX/environment.plist`):

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>PATH</key>
<string>/opt/local/Library/Frameworks/Python.framework/Versions/2.7/site-packages/opt/local/bin:/opt/local/sbin:/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/opt/X11/bin:/usr/X11/bin</string>
<key>PYTHONPATH</key>
<string>/opt/local/Library/Frameworks/Python.framework/Versions/2.7/site-packages</string>
</dict>
</plist>
```

# Installation scripts #

Clone to `~/.aprl`:

```sh
$ git clone https://bitbucket.org/stakahama/aprl-shell ~/.aprl
```

## mac ##

Rename macports python to `/usr/bin/python` (original saved to `/usr/bin/python.orig`), and append `source` call to `~/.aprl/.bashrc_osx` in `~/.bashrc`:

```sh
$ cd ~/.aprl
$ sudo sh rename-python.sh
$ sh append-vars.sh
```

Start new shell (so that `~/.bashrc` is reloaded; otherwise just `source ~/.aprl/.bashrc_osx`) and then create `~/.launch.conf` (also creates `~/.MacOSX/environment.plist`, which is the same file) with the included python script:

```sh
$ python make-plist.py
```
