#!/bin/sh

if [ -f /opt/local/bin/python ]; then
    mv /usr/bin/python /usr/bin/python.orig && \
	ln -svf /opt/local/bin/python /usr/bin/python
fi
