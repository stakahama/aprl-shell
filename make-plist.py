#!/usr/bin/env python

## need to log out and log back in after plist modification
##   to be loaded for GUI apps (e.g., Emacs.app)
## run from terminal rather than Emacs shell or emulator

###_* libraries
import os
import shutil

###_* user inputs

varlist = ['PATH','PYTHONPATH'] # environmental variables

###_* formatting

###_ . define functions
## copied from old Mac
preamble = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'''
header = '''
<plist version="1.0">
<dict>
'''
footer = '''
</dict>
</plist>
'''
def format(var):
    return '<key>%s</key>\n<string>%s</string>' % (var,os.environ[var])

###_ . apply 
contents = '\n'.join(map(format,varlist))

###_ . create paths and file names
osxpath = os.path.expanduser('~/.MacOSX')
if not os.path.exists(osxpath):
    os.mkdir(osxpath)

plistfile = os.path.join(osxpath,'environment.plist')
launchfile = os.path.expanduser('~/.launchd.conf')

###_* write
## create new
with open(plistfile,'w') as fout:
    fout.write(preamble)
    fout.write(header)
    fout.write(contents)
    fout.write(footer)

shutil.copy2(plistfile,launchfile)
